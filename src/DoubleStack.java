import java.util.LinkedList;
import java.util.StringTokenizer;

public class DoubleStack {

   private LinkedList<Double> linkedList;  // new LinkedList

   public static void main(String[] args) {
      // TODO!!! Your tests here!
   }

   // Simple constructor
   public DoubleStack() {
      linkedList = new LinkedList<Double>();
   }

   // Copy current stack
   @Override
   public Object clone() throws CloneNotSupportedException {

      DoubleStack newStack = new DoubleStack();

      for(int i = 0; i <= this.linkedList.size() - 1; i++) {
         newStack.linkedList.add(i, this.linkedList.get(i));
      }
      return newStack;
   }

   // Check if stack is empty
   public boolean stEmpty() {
      return this.linkedList.isEmpty();
   }

   // Add element to stack (push)
   public void push(double a) {
      this.linkedList.addFirst(a);
   }

   // Remove element from stack (pop)
   public double pop() {
      if (stEmpty()) {
         throw new RuntimeException("Not enough numbers to perform operation");
      } else {
         return this.linkedList.poll();
      }
   }

   // Arithmetic operations: + - * /
   public void op(String s) {

      double op2;
      double op1;

      if (this.linkedList.size() >= 2) {
         op2 = pop();
         op1 = pop();
      } else throw new RuntimeException("Not enough numbers to perform operation");

      if (s.equals("+")) {
         push(op1 + op2);
      } else if (s.equals("-")) {
         push(op1 - op2);
      } else if (s.equals("*")) {
         push(op1 * op2);
      } else if (s.equals("/")) {
         push(op1 / op2);
      } else throw new RuntimeException(s + ": Illegal operator. Please use +, -, * or /");
   }

   // View first element of stack without removing it
   public double tos() {
      if (stEmpty())
         throw new RuntimeException("Stack empty");
      return this.linkedList.peekFirst();
   }

   // Compares two DoubleStack objects if they are equal
   @Override
   public boolean equals(Object o) {

      if (this.linkedList.size() != ((DoubleStack)o).linkedList.size()) return false;  // Check size

      for(int i = 0; i < this.linkedList.size(); i++) {
         if (!this.linkedList.get(i).equals(((DoubleStack) o).linkedList.get(i))) return false;    // Check list elements
      }
      return true;
   }

   @Override
   public String toString() {
      if (stEmpty()) return "Stack empty!";
      StringBuilder a = new StringBuilder(); // StringBuilder used instead of StringBuffer, should be more efficient
      for (int i = this.linkedList.size() - 1; i >= 0; i--) {
         a.append(String.valueOf(this.linkedList.get(i)));
         a.append(" ");
      }
      return a.toString();
   }

   public static double interpret(String pol) {

      DoubleStack newRPN = new DoubleStack();               // New DoubleStack to hold RPN operands

      StringTokenizer tokenizer = new StringTokenizer(pol);  // StringTokenizer is used to split string

      int countTokens = tokenizer.countTokens();

      while (tokenizer.hasMoreTokens()) {

         String temp = tokenizer.nextToken();

         double nr1;
         double nr2;

         if (newRPN.linkedList.size() > 0 && !isDouble(temp)) {
            if (temp.equals("+")) {
               nr1 = newRPN.pop();
               nr2 = newRPN.pop();
               newRPN.push(nr2 + nr1);

            } else if (temp.equals("-")) {
               nr1 = newRPN.pop();
               nr2 = newRPN.pop();
               newRPN.push(nr2 - nr1);

            } else if (temp.equals("*")) {
               nr1 = newRPN.pop();
               nr2 = newRPN.pop();
               newRPN.push(nr2 * nr1);

            } else if (temp.equals("/")) {
               nr1 = newRPN.pop();
               nr2 = newRPN.pop();
               newRPN.push(nr2 / nr1);

            } else
               throw new RuntimeException("Illegal operator used in expression: " + pol);     // => Operator in the beginning or not + - * /

         } else try {
            newRPN.push(Double.parseDouble(temp));
         } catch (RuntimeException e) {
            throw new RuntimeException("Illegal number format \"" + temp + "\". Number must be decimal");   // => Cannot parse to double
         }

         if (countTokens > 1 && tokenizer.countTokens() == 0 && isDouble(temp)) {
            throw new RuntimeException("Last entry in expression " + pol + " must not be a number");  // => Last entry is a number
         }

      }
      return newRPN.pop();
   }

   // Small method to test if string can be parsed to double
   public static boolean isDouble(String a) {
      try {
         Double.parseDouble(a);
         return true;
      } catch (NumberFormatException e) {
         return false;
      }
   }
}



